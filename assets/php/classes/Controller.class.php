<?php
    // Class holding the main logic of the website

    declare(strict_types = 1);

    namespace Vendor;

    class Controller extends DBConnector {
        private $products;

        public function __construct(string $host, string $dbName, string $username, string $password) {
            parent::__construct($host, $dbName, $username, $password);

            $this->products = array();
        }

        public function init() {
            $items = $this->pdo->query('SELECT * FROM items')->fetchAll();

            foreach($items as $item) {
                $this->products[$item['itemSKU']] = Product\Factory::createProduct($item['itemSKU'], $item['itemName'], $item['itemPrice'], $item['itemType'], $item['itemAttribute']);
            }
        }

        public function showProducts() {
            foreach($this->products as $product) {
                echo $product->showProduct();
            }
        }

        public function insert(Product\Product $product) {
            $product->insert($this->pdo);
        }

        public function delete(array $items) {
            foreach ($items as $item) {
                $this->products[$item]->delete($this->pdo);

                unset($this->products[$item]);
            }
        }
    }
?>