<?php
    // Class responsible for connecting to the database

    declare(strict_types = 1);

    namespace Vendor;

    abstract class DBConnector {
        protected $pdo;
        private $host, $dbName, $username, $password;

        public function __construct(string $host, string $dbName, string $username, string $password) {
            $this->host = $host;
            $this->dbName = $dbName;
            $this->username = $username;
            $this->password = $password;
        }

        public function connect() {
            $temp = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
            
            $this->pdo = new \PDO($temp, $this->username, $this->password);

            $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        }
    }
?>