<?php
    // Abstract class which defines what all products have to implement / have in common

    declare(strict_types = 1);

    namespace Vendor\Product;

    abstract class Product {
        private $sku, $name, $price, $type;

        public function __construct(string $sku, string $name, string $price, string $type) {
            $this->sku = $sku;
            $this->name = $name;
            $this->price = $price;
            $this->type = $type;
        }

        public function getSKU() {
            return $this->sku;
        }

        protected function dump() {
            return array($this->sku, $this->name, $this->price, $this->type);
        }

        protected function generateHTML(string $class) {
            return '<input class="' . $class . '__checkbox" name="items[]" value="' . $this->sku . '" type="checkbox">
                    <p class="' . $class . '__text">' . $this->sku . '</p>
                    <p class="' . $class . '__text">' . $this->name . '</p>
                    <p class="' . $class . '__text">' . $this->price . ' $</p>';
        }

        public function showProduct(string $class = 'items__form__item') {
            return '<div class="' . $class . '">' . $this->generateHTML($class) . '</div>';
        }

        public function insert(\PDO $pdo) {
            $stmt = $pdo->prepare('INSERT INTO items (itemSKU, itemName, itemPrice, itemType, itemAttribute) VALUES (?, ?, ?, ?, ?)');

            $stmt->execute($this->dump());
        }

        public function delete(\PDO $pdo) {
            $stmt = $pdo->prepare('DELETE FROM items WHERE itemSKU = ?');

            $stmt->execute(array($this->sku));
        }
    }
?>