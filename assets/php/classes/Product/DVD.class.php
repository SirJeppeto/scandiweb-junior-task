<?php
    // Class which describes products of type furniture
    
    declare(strict_types = 1);

    namespace Vendor\Product;

    class DVD extends Product {
        private $size;

        public function __construct(string $sku, string $name, string $price, string $type, string $size) {
            parent::__construct($sku, $name, $price, $type);

            $this->size = $size;
        }

        public function dump() {
            $array = parent::dump();

            $array[] = $this->size;

            return $array;
        }

        protected function generateHTML(string $class) {
            return parent::generateHTML($class) . '<p class="' . $class . '__text">Size: ' . $this->size . ' MB</p>';
        }
    }
?>