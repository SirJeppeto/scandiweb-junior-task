<?php
    // Class which handles the creation of products

    declare(strict_types = 1);

    namespace Vendor\Product;

    abstract class Factory {
        private const TYPES = [
            'dvd' => 'self::createDVD',
            'book' => 'self::createBook',
            'furniture' => 'self::createFurniture'
        ];

        public static function createDVD(string $sku, string $name, string $price, string $type, string $attribute) {
            return new DVD($sku, $name, $price, $type, $attribute);
        }

        public static function createBook(string $sku, string $name, string $price, string $type, string $attribute) {
            return new Book($sku, $name, $price, $type, $attribute);
        }

        public static function createFurniture(string $sku, string $name, string $price, string $type, string $attribute) {
            return new Furniture($sku, $name, $price, $type, $attribute);
        }

        public static function createProduct(string $sku, string $name, string $price, string $type, string $attribute) {
            return call_user_func_array(self::TYPES[$type], array($sku, $name, $price, $type, $attribute));
        }
    }
?>