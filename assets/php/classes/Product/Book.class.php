<?php
    // Class which describes products of type furniture
    
    declare(strict_types = 1);

    namespace Vendor\Product;

    class Book extends Product {
        private $weight;

        public function __construct(string $sku, string $name, string $price, string $type, string $weight) {
            parent::__construct($sku, $name, $price, $type);

            $this->weight = $weight;
        }

        public function dump() {
            $array = parent::dump();

            $array[] = $this->weight;

            return $array;
        }

        protected function generateHTML(string $class) {
            return parent::generateHTML($class) . '<p class="' . $class . '__text">Weight: ' . $this->weight . 'KG</p>';
        }
    }
?>