<?php
    // Class which describes products of type furniture

    declare(strict_types = 1);
    
    namespace Vendor\Product;

    class Furniture extends Product {
        private $dimension;

        public function __construct(string $sku, string $name, string $price, string $type, string $dimension) {
            parent::__construct($sku, $name, $price, $type);

            $this->dimension = $dimension;
        }

        public function dump() {
            $array = parent::dump();

            $array[] = $this->dimension;

            return $array;
        }

        protected function generateHTML(string $class) {
            return parent::generateHTML($class) . '<p class="' . $class . '__text">Dimension: ' . $this->dimension . '</p>';
        }
    }
?>