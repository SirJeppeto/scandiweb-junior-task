// object which helps with dynamic creation of form inputs
const dynamicForm = {
    'dvd': {
        text: [
            'Size (MB)'
        ],
        description: 'Please provide the size of the DVD in MB'
    },
    'book': {
        text: [
            'Weight (KG)'
        ],
        description: 'Please provide the weight of the book in KG'
    },
    'furniture': {
        text: [
            'Height (CM)',
            'Width (CM)',
            'Length (CM)'
        ],
        description: 'Please provide the dimensions of the piece of furniture in CM'
    }
};

// redirecting page
const redirect = (link) => {
    window.location.href = link;
}

// onclick function for Save button
const submitForm = (formID) => {
    if(formValid()) {
        const form = document.querySelector(formID);

        form.submit();
    }
}

// form validation
const formValid = () => {
    const input = document.querySelectorAll("input");

    for(let i = 0; i < input.length; i++) {
        
        // checking if input field is empty
        if(input[i].value === undefined || input[i].value.trim() === "") {
            input[i].parentElement.setAttribute('data-notification', 'Please, submit required data');
            
            return false;
        }

        input[i].value = input[i].value.trim();
    }

    const select = document.querySelector("select");

    // checking if option was selected in Type Switcher
    if(select.options[select.selectedIndex].value === "") {
        select.parentElement.setAttribute('data-notification', 'Please, submit required data');

        return false;
    }

    const numbers = document.querySelectorAll(".input__form__part__input--number");

    for(let i = 0; i < numbers.length; i++) {
        numbers[i].value = numbers[i].value.replace(",", ".");

        // checking if value is a number
        if(isNaN(numbers[i].value) || parseFloat(numbers[i].value) < 0.00) {
            numbers[i].parentElement.setAttribute('data-notification', 'Please, provide the data of indicated type');
            
            return false;
        }
    }

    return true;
}

// oninput function which erases validation message
const activeInput = (item) => {
    item.parentElement.setAttribute('data-notification', '');
}

// onchange function for the Type Switcher select
const typeChanged = (select) => {
    const selectedValue = select.options[select.selectedIndex].value;
    const selectDIV = document.querySelector('#dynamicForm');

    let temp = '';

    dynamicForm[selectedValue].text.forEach(element => {
        temp += `<div class="input__form__part" data-notification="">
                    <label class="input__form__part__label" for="itemAttribute[]">${element}</label>
                    <input class="input__form__part__input input__form__part__input--number" type="text" name="itemAttribute[]" oninput="activeInput(this);" />
                </div>`;
    });

    temp += `<p class="input__form__text">${dynamicForm[selectedValue].description}</p>`;

    selectDIV.innerHTML = temp;
}