<?php
    // The 'Add Product' webpage
    
    declare(strict_types = 1);

    namespace Vendor;

    include '../../assets/php/includes/autoload.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Product Add</title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../../assets/css/add.css"/>
        <script type="text/javascript" src="../../assets/js/app.js"></script>
    </head>
    <body>
        <div class="top">
            <p class="top__text">Product Add</p>
            <div class="top__buttons">
                <button class="top__buttons__save" onclick="submitForm('#add');">Save</button>
                <button class="top__buttons__cancel" onclick="redirect('../list/');">Cancel</button>
            </div>
        </div>
        <hr>
        <div class="input">
            <form class="input__form" id="add" method="post" action="../list/">
                <div class="input__form__part" data-notification="">
                    <label class="input__form__part__label" for="itemSKU">SKU</label>
                    <input class="input__form__part__input" type="text" name="itemSKU" oninput="activeInput(this);" />
                </div>
                <div class="input__form__part" data-notification="">
                    <label class="input__form__part__label" for="itemName">Name</label>
                    <input class="input__form__part__input" type="text" name="itemName" oninput="activeInput(this);" />
                </div>
                <div class="input__form__part" data-notification="">
                    <label class="input__form__part__label" for="itemPrice">Price ($)</label>
                    <input class="input__form__part__input input__form__part__input--number" type="text" name="itemPrice" oninput="activeInput(this);" />
                </div>
                <div class="input__form__part" data-notification="">
                    <label class="input__form__part__label input__form__part__label--type" for="itemType">Type Switcher</label>
                    <select class="input__form__part__input" name="itemType" oninput="activeInput(this);" onchange="typeChanged(this);">
                        <option value="" selected disabled>Type Switcher</option>
                        <option value="dvd">DVD</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>
                    </select>
                </div>
                <div id="dynamicForm"></div>
            </form>
        </div>
        <hr>
        <div class="bottom">
            <p class="bottom__text">Scandiweb Test Assignment</p>
        </div>
    </body>
</html>