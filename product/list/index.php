<?php
    // The 'List Products' webpage
    
    declare(strict_types = 1);

    namespace Vendor;

    include '../../assets/php/includes/autoload.inc.php';

    $controller = new Controller('localhost', 'id16618700_items', 'id16618700_sirjeppeto', 'Josip1234@rit94');

    try {
        $controller->connect();
    } catch(\Exception $e) {
        echo "Problem connecting to database...";
        exit;
    }

    // Adds item to database
    if(isset($_POST['itemType'])) {
        $controller->insert(Product\Factory::createProduct($_POST['itemSKU'], $_POST['itemName'], $_POST['itemPrice'], $_POST['itemType'], implode('x', $_POST['itemAttribute'])));
    }

    $controller->init();

    // Deletes items from database
    if(isset($_POST['items'])) {
        $controller->delete($_POST['items']);
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Product List</title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../../assets/css/list.css"/>
        <script type="text/javascript" src="../../assets/js/app.js"></script>
    </head>
    <body>
        <div class="top">
            <p class="top__text">Product List</p>
            <div class="top__buttons">
                <button class="top__buttons__add" onclick="redirect('../add/');">Add</button>
                <button class="top__buttons__delete" type="submit" form="list">Mass delete</button>
            </div>
        </div>
        <hr>
        <div class="items">
            <form class="items__form" id="list" method="post" action="">
                <?php
                    $controller->showProducts();
                ?>
            </form>
        </div>
        <hr>
        <div class="bottom">
            <p class="bottom__text">Scandiweb Test Assignment</p>
        </div>
    </body>
</html>